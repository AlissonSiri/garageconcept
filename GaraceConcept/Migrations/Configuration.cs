namespace GarageConcept.Migrations
{
    using GarageConcept.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public sealed class Configuration : DbMigrationsConfiguration<GarageConcept.Data.Context.GarageConceptContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(GarageConcept.Data.Context.GarageConceptContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            var userManager = new ApplicationUserManager(new UserStore<User>(context));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            string role = Roles.SiteAdmin.GetStringValue();
            string password = "@dmin123";
            string email = "admin@gara.com.br";

            if (!roleManager.RoleExists(role))
            {
                roleManager.Create(new IdentityRole(role));
            }

            if (!context.Users.Any(u => u.Email == email))
            {
                User admin = new User("999999999");
                admin.Name = "Admin";
                admin.UserName = "Admin";
                admin.Email = "admin@garageconcept.com.br";
                admin.EmailConfirmed = true;

                var adminresult = userManager.Create(admin, password);

                //Add User Admin to Role Admin
                if (adminresult.Succeeded)
                    userManager.AddToRole(admin.Id, role);
            }

        }
    }
}
