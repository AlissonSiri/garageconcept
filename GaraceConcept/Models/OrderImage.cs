﻿namespace GarageConcept.Models
{
    public class OrderImage : BaseEntity
    {

        public int OrderId { get; set; }

        public string FileName { get; set; }

        public int DataLength { get; set; }

        public byte[] Data { get; set; }

        public virtual Order Order { get; set; }

        protected OrderImage()
        {

        }

        public OrderImage(string fileName, byte[] data): this()
        {
            this.FileName = fileName;
            this.Data = data;
            this.DataLength = data.Length;
        }
    }
}