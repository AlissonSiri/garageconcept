﻿using System;
using System.Collections.Generic;

namespace GarageConcept.Models
{
    public class Order: BaseEntity
    {

        public int VehicleId { get; set; }

        public string OwnerId { get; set; }

        public OrderStatus Status { get; set; }

        public virtual Vehicle Vehicle { get; set; }

        public virtual User OwnerAtOrderDate { get; set; }

        public string Description { get; set; }

        public virtual ICollection<OrderImage> Images { get; set; }

        public virtual ICollection<OrderHistory> Historic { get; set; }

        protected Order()
        {
            this.Status = OrderStatus.New;
            this.Images = new HashSet<OrderImage>();
            this.Historic = new HashSet<OrderHistory>();
        }

        public Order(Vehicle vehicle): this()
        {
            Vehicle = vehicle;
            OwnerAtOrderDate = vehicle.Owner;
        }

        public void AddImage(string fileName, byte[] data)
        {
            this.Images.Add(new OrderImage(fileName, data));
        }

        public void AddToHistory(string description)
        {
            this.Historic.Add(new OrderHistory(description));
        }

    }
}