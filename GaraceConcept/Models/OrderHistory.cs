﻿namespace GarageConcept.Models
{
    public class OrderHistory: BaseEntity
    {

        public string Description { get; set; }

        public int OrderId { get; set; }

        public virtual Order Order { get; set; }

        protected OrderHistory()
        {

        }

        public OrderHistory(string description)
        {
            this.Description = description;
        }

    }
}