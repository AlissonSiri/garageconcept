﻿using GarageConcept.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace GarageConcept.Data.EntityConfig
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {

        public UserConfiguration()
        {
            Property(p => p.Name).IsRequired().HasMaxLength(255);
            
            Property(p => p.PictureUrl).HasMaxLength(1000);

            HasOptional(f => f.ProfileImage)
            .WithRequired(s => s.Owner);

            Property(p => p.Email).IsRequired()
            .HasMaxLength(255);

            Property(p => p.Cpf).IsRequired()
            .HasMaxLength(11);

            HasIndex(e => new { e.Email }).IsUnique();
            HasIndex(e => new { e.Cpf }).IsUnique();

        }

    }
}