﻿using GarageConcept.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace GarageConcept.Data.EntityConfig
{
    public class OrderConfiguration : EntityTypeConfiguration<Order>
    {

        public OrderConfiguration()
        {
            Property(o => o.Description).HasMaxLength(1000);
            HasRequired(o => o.OwnerAtOrderDate).WithMany().HasForeignKey(o => o.OwnerId);
            HasMany(o => o.Historic).WithRequired(h => h.Order).HasForeignKey(o => o.OrderId);
        }

    }
}