﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace System.Web.Mvc
{
    public class AuthorizeMessageAttribute : AuthorizeAttribute
    {
        private bool _override = false;
        public string Message { get; set; }
        public bool Override { get { return _override; } set { _override = value; } }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            this.PrepareMessage();
            var action = filterContext.ActionDescriptor;
            if (!this.Override && action.IsDefined(typeof(AuthorizeAttribute), true))
                return;
            base.OnAuthorization(filterContext);
        }

        private void PrepareMessage()
        {
            if (!this.Message.IsNullOrEmpty())
                return;
            var issue = "estar logado";
            List<string> roles = this.Roles.Split(',').ToList();
            roles.ForEach(r => r.Trim());
            if (roles.Count > 0)
                issue = roles.JoinWithLast(", ", " ou ");
            this.Message = String.Format("Necessário {0} para continuar", issue);
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Controller.TempData["AuthorizationMessage"] = this.Message;
            base.HandleUnauthorizedRequest(filterContext);
        }

    }
}