﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GarageConcept.Helpers.Attributes.DataAnnotations
{

    public class RegexReplaceAttribute : ValidationAttribute
    {

        public string ReplaceThis { get; set; }

        public string WithThis { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string text = value.ToString();
            var sb = new StringBuilder(text);

            foreach (char c in ReplaceThis)
                sb.Replace(c.ToString(), WithThis);
            
            validationContext
            .ObjectType
            .GetProperty(validationContext.MemberName)
            .SetValue(validationContext.ObjectInstance, sb.ToString(), null);
            
            return ValidationResult.Success;
        }

    }

}