﻿namespace System.Web.Mvc
{
    public class AuthorizeAdminMessageAttribute : AuthorizeMessageAttribute
    {

        public AuthorizeAdminMessageAttribute(): base()
        {
            this.Roles = GarageConcept.Models.Roles.SiteAdmin.GetStringValue();
        }

    }
}