﻿using GarageConcept.Helpers.Attributes.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GarageConcept.ViewModels.Orders
{
    public class EditOrderViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Por favor, preencha o chassi!")]
        [DisplayName("Chassi")]
        public string Chassis { get; set; }

        [Required(ErrorMessage = "Por favor, preencha o model!")]
        [DisplayName("Modelo")]
        public string VehicleModel { get; set; }

        [Required(ErrorMessage = "Por favor, digite o nome do proprietário!")]
        [DisplayName("Proprietário")]
        public string OwnerName { get; set; }

        [Required(ErrorMessage = "Por favor, digite o CPF do proprietário!")]
        [DisplayName("CPF Proprietário")]
        [RegexReplace(ReplaceThis = ".-", WithThis = "")]
        public string OwnerCpf { get; set; }

        [Required]
        [DisplayName("Atualizar Proprietário do Veículo")]
        public bool UpdateVehicleOwner { get; set; }

        [MaxLength(1000, ErrorMessage = "Por favor, digite no máximo {0} caracteres.")]
        [DisplayName("Descrição")]
        public string Description { get; set; }

    }
}