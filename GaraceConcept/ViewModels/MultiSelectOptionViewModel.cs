﻿namespace GarageConcept.ViewModels
{
    public class MultiSelectOptionViewModel
    {

        public int Id { get; set; }

        public string Name { get; set; }

        public bool Selected { get; set; }

    }
}