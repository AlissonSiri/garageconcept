﻿using GarageConcept.Helpers.Attributes.DataAnnotations;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GarageConcept.ViewModels
{
    public class UserViewModel
    {
        [Key]
        public string ID { get; set; }

        [Required]
        [StringLength(255, MinimumLength = 2, ErrorMessage = "")]
        [DisplayName("Common_Name")]
        public string Name { get; set; }

        public string UserName { get; set; }

        [Required]
        [StringLength(255, MinimumLength = 2, ErrorMessage = "")]
        [EmailAddress]
        public string Email { get; set; }

        public string Occupation { get; set; }

        public bool IsAdmin { get; set; }

        public bool IsInactive { get; set; }

        public string PictureUrl { get; set; }

        public byte[] PictureBytes { get; set; }

        public DateTime RegisterDate { get; set; }

        public DateTime UpdateDate { get; set; }

        public string RegisterDateDisplay { get; set; }

        public bool IsBlocked { get; set; }

        public string ProfileUrl { get; set; }

        public string StatusPersonal { get; set; }

        public string ExpireDateDisplay { get; set; }

        public DateTime ExpireDate { get; set; }

        public string Phone { get; set; }
        
    }
}