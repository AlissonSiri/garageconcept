﻿using GarageConcept.Data.Context;
using GarageConcept.Helpers;
using GarageConcept.ViewModels;
using GarageConcept.ViewModels.Images;
using GarageConcept.ViewModels.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity;

namespace GarageConcept.Controllers.API
{
    [Authorize]
    [RoutePrefix("api/orders")]
    public class OrdersApiController : ApiController
    {

        [HttpPost]
        [Route("list", Name = "Orders.Get")]
        public async Task<IHttpActionResult> GetAll(BootgridRequestViewModel request)
        {
            using (var db = new GarageConceptContext())
            {
                var query = db.Orders.Where(o => o.Status != Models.OrderStatus.Deleted).AsQueryable();
                if (!AuthHelper.IsAdmin())
                {
                    var userId = AuthHelper.GetLoggedUserId();
                    query = query.Where(o => o.OwnerId == userId);
                }
                var orders = query.Select(o => new ListOrderViewModel
                {
                    Id = o.Id,
                    Register = o.Register,
                    Status = o.Status,
                    PlateNumber = o.Vehicle.PlateNumber,
                    ChassisNumber = o.Vehicle.ChassisNumber,
                    Model = o.Vehicle.Model,
                    OwnerName = o.OwnerAtOrderDate.Name,
                    OwnerCpf = o.OwnerAtOrderDate.Cpf,
                    OwnerEmail = o.OwnerAtOrderDate.Email
                });

                if (!string.IsNullOrWhiteSpace(request.searchPhrase))
                    orders = orders.Where(o => o.ChassisNumber.Contains(request.searchPhrase)
                    || o.Model.Contains(request.searchPhrase)
                    || o.OwnerCpf.Contains(request.searchPhrase)
                    || o.OwnerEmail.Contains(request.searchPhrase)
                    || o.OwnerName.Contains(request.searchPhrase)
                    || o.PlateNumber.Contains(request.searchPhrase)
                    || o.Register.ToString() == request.searchPhrase
                    || o.Id.ToString() == request.searchPhrase
                    );

                var model = await orders.OrderByDescending(o => o.Id).ApplyFilter(request);

                foreach (var order in model.rows)
                    order.Format();

                return Ok(model);

            }
        }

        [HttpPost]
        [Route("{orderId:int}/history", Name = "Orders.GetHistory")]
        public async Task<IHttpActionResult> GetHistory(int orderId, BootgridRequestViewModel request)
        {
            using (var db = new GarageConceptContext())
            {
                var query = db.OrdersHistory.Where(o => o.OrderId == orderId).AsQueryable();
                if (!AuthHelper.IsAdmin())
                {
                    var userId = AuthHelper.GetLoggedUserId();
                    query = query.Where(o => o.Order.OwnerId == userId);
                }
                var orders = query.Select(o => new
                {
                    o.Id,
                    CreatedAt = o.Register,
                    o.Description
                });

                if (!string.IsNullOrWhiteSpace(request.searchPhrase))
                    orders = orders.Where(o => o.Description.Contains(request.searchPhrase)
                    || o.CreatedAt.ToString() == request.searchPhrase
                    || o.Id.ToString() == request.searchPhrase
                    );

                var model = await orders.OrderByDescending(o => o.Id).ApplyFilter(request);

                return Ok(model);
            }
        }

        [HttpGet]
        [Route("{id:int}/images", Name = "Orders.GetImages")]
        public async Task<IHttpActionResult> GetImages(int id)
        {
            using (var db = new GarageConceptContext())
            {
                var q = db.OrderImages
                    .Where(i => i.OrderId == id).AsQueryable();

                if (!AuthHelper.IsAdmin())
                {
                    var userId = AuthHelper.GetLoggedUserId();
                    q = q.Where(o => o.Order.OwnerId == userId);
                }

                var files = await q.Select(file => new ImageViewModel
                    {
                        Id = file.Id,
                        Name = file.FileName,
                        Size = file.DataLength,
                        CreatedAt = file.Register
                    }).OrderByDescending(f => f.CreatedAt).ToListAsync();

                foreach (var file in files)
                {
                    file.Url = Url.Link("Default", new { controller = "Images", action = "GetFile", id = file.Id });
                    file.ThumbnailUrl = Url.Link("Default", new { controller = "Images", action = "GetFile", id = file.Id, thumbnail = true });
                    file.DeleteUrl = Url.Link("Default", new { controller = "Images", action = "DeleteFile", id = file.Id });
                }

                return Ok(files);

            }
        }

        [Authorize(Roles = "ADMIN")]
        [HttpDelete]
        [Route("delete/{id:int}", Name = "Orders.Delete")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            using (var db = new GarageConceptContext())
            {
                var order = await db.Orders.FindAsync(id);
                order.Status = Models.OrderStatus.Deleted;
                order.AddToHistory("OS excluída");
                await db.SaveChangesAsync();
                return Ok();
            }
        }

    }
}