﻿using GarageConcept.Data.Context;
using GarageConcept.Helpers;
using GarageConcept.Models;
using GarageConcept.ViewModels;
using GarageConcept.ViewModels.Vehicles;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace GarageConcept.Controllers.API
{
    [Authorize(Roles = "ADMIN")]
    [RoutePrefix("api/vehicles")]
    public class VehiclesApiController : ApiController
    {

        [HttpPost]
        [Route("list", Name = "Vehicles.Get")]
        public async Task<IHttpActionResult> GetAll(BootgridRequestViewModel request)
        {
            using (var db = new GarageConceptContext())
            {
                var query = db.Vehicles.AsQueryable();
                if (!AuthHelper.IsAdmin())
                {
                    var userId = AuthHelper.GetLoggedUserId();
                    query = query.Where(o => o.OwnerId == userId);
                }
                var vehicles = query.Select(v => new ListVehicleViewModel
                {
                    Id = v.Id,
                    Register = v.Register,
                    PlateNumber = v.PlateNumber,
                    ChassisNumber = v.ChassisNumber,
                    Model = v.Model,
                    OwnerName = v.Owner.Name,
                    OwnerCpf = v.Owner.Cpf,
                    OwnerEmail = v.Owner.Email,
                    OSCount = v.Orders.Where(o => o.Status != OrderStatus.Deleted).Count()
                });

                if (!string.IsNullOrWhiteSpace(request.searchPhrase))
                {
                    vehicles = vehicles.Where(o => o.ChassisNumber.Contains(request.searchPhrase)
                    || o.Model.Contains(request.searchPhrase)
                    || o.OwnerCpf.Contains(request.searchPhrase)
                    || o.OwnerEmail.Contains(request.searchPhrase)
                    || o.OwnerName.Contains(request.searchPhrase)
                    || o.PlateNumber.Contains(request.searchPhrase)
                    || o.Register.ToString() == request.searchPhrase
                    || o.Id.ToString() == request.searchPhrase
                    );
                }

                var model = await vehicles.OrderByDescending(o => o.Id).ApplyFilter(request);

                foreach (var order in model.rows)
                {
                    order.Format();
                }

                return Ok(model);

            }
        }

        [HttpGet]
        [Route("{plate}/details", Name = "Vehicles.GetDetails")]
        public async Task<IHttpActionResult> GetDetails(string plate)
        {
            using (var db = new GarageConceptContext())
            {
                var vehicle = await db.Vehicles.Where(v => v.PlateNumber == plate).Select(v => new
                {
                    v.ChassisNumber,
                    v.Model,
                    v.Owner.Cpf,
                    v.Owner.Name
                }).FirstOrDefaultAsync();
                return Ok(vehicle);
            }
        }

        [HttpDelete]
        [Route("delete/{id:int}", Name = "Vehicles.Delete")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            using (var db = new GarageConceptContext())
            {
                bool hasOrders = await db.Orders.Where(o => o.VehicleId == id).AnyAsync();
                if (hasOrders)
                {
                    throw new InvalidOperationException("Esse veículo contém OS's. Exclua primeiro as OS's.");
                }
                var vehicle = await db.Vehicles.FindAsync(id);
                await db.SaveChangesAsync();
                return Ok();
            }
        }

    }
}