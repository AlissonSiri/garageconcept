﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Data.Entity;
using GarageConcept.ViewModels.Account;
using GarageConcept.Models;
using GarageConcept.Data.Context;
using GarageConcept.Helpers;

namespace GarageConcept.Controllers
{

    [Authorize]
    public class AccountController : BaseController
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [AllowAnonymous]
        public async Task<ActionResult> Login(string returnUrl)
        {
            if (AuthHelper.IsAuthenticated())
            {
                using (var db = new GarageConceptContext())
                {
                    var userId = AuthHelper.GetLoggedUserId();
                    var user = await db.Users.FirstAsync(u => u.Id == userId);
                    await db.SaveChangesAsync();
                }
                return RedirectToAction("Show", "Profile");
            }
            if (TempData.ContainsKey("IdentityResult"))
                AddErrors(TempData["IdentityResult"] as IdentityResult);
            if (TempData.ContainsKey("AuthorizationMessage"))
                ViewBag.AuthorizationMessage = TempData["AuthorizationMessage"];
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [AllowAnonymous]
        public async Task<ActionResult> ForceLogin(string userId)
        {
            var user = await UserManager.FindByIdAsync(userId);
            await SignInManager.SignInAsync(user, true, true);

            return RedirectToLocal();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(returnUrl, model);
            }

            using (var db = new GarageConceptContext())
            {
                // find by email or username
                var user = await db.Users.Where(u => u.Email == model.Login || u.UserName == model.Login).FirstOrDefaultAsync();
                // if not found, find by cpf
                if (user == null)
                {
                    string cpf = new String(model.Login.Where(Char.IsDigit).ToArray());
                    user = await db.Users.Where(u => u.Cpf == cpf).FirstOrDefaultAsync();
                }
                // if not found, try converting the login to int (to check if the user entered an OS number) and look for it
                if (user == null && int.TryParse(model.Login, out int os))
                {
                    user = await db.Orders.Where(o => o.Id == os).Select(o => o.OwnerAtOrderDate).FirstOrDefaultAsync();
                }

                if (user == null)
                {
                    ModelState.AddModelError("", "Tentativa de Login inválida.");
                    return View(returnUrl, model);
                }

                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, change to shouldLockout: true
                var result = await SignInManager.PasswordSignInAsync(user.UserName, model.Password, model.RememberMe, shouldLockout: false);
                switch (result)
                {
                    case SignInStatus.Success:
                        return RedirectToLocal(returnUrl);
                    case SignInStatus.LockedOut:
                        return View("Lockout");
                    case SignInStatus.Failure:
                    default:
                        ModelState.AddModelError("", "Tentativa de Login inválida.");
                        return View(model);
                }
            }
        }
        
        [AllowAnonymous]
        public ActionResult EmailSent()
        {
            return View();
        }

        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (AuthHelper.IsAuthenticated())
                return RedirectToAction("Show", "Profile");

            if (userId == null || code == null)
                return View("Error");

            var result = await UserManager.ConfirmEmailAsync(userId, code);
            if (!result.Succeeded)
                return View("Error");

            var user = await UserManager.FindByIdAsync(userId);

            await SignInManager.SignInAsync(user, isPersistent: true, rememberBrowser: true);
            return RedirectToAction("Show", "Profile");
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            if (AuthHelper.IsAuthenticated())
                return RedirectToAction("Show", "Profile");

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(EmailViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByEmailAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist
                    return View(model);
                }

                string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                await UserManager.SendEmailAsync(user.Id, "Redefinir senha", "Por favor, redefina sua senha clicando <a href=\"" + callbackUrl + "\">aqui</a>");
                return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            return View(model);
        }

        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            if (AuthHelper.IsAuthenticated())
                return RedirectToAction("Show", "Profile");

            return View();
        }

        [AllowAnonymous]
        public ActionResult ResetPassword(string userId, string code, string email)
        {
            return code == null ? View("Error") : View(new ResetPasswordViewModel() { Code = code, UserId = userId });
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByIdAsync(model.UserId);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult EmailResend(string email)
        {
            if (AuthHelper.IsAuthenticated())
                return RedirectToAction("EmailSent");
            return View(new EmailViewModel() { Email = email });
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EmailResend(EmailViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = UserManager.FindByEmail(model.Email);
                if (user.EmailConfirmed)
                    return View();

                string code = UserManager.GenerateEmailConfirmationToken(user.Id);
                var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                await UserManager.SendEmailAsync(user.Id, "Confirmar criação de usuário", "Por favor, apenas queremos confirmar se você tem acesso ao email utilizado para registrar uma conta, é para sua <strong>própria segurança</strong>. Confirme clicando <a href=\"" + callbackUrl + "\">aqui</a>");
                return RedirectToAction("EmailSent");
            }

            // If we got this far, something failed, redisplay form
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return Redirect(Url.Content("~/"));
        }

        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }

            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}