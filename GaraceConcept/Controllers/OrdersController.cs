﻿using GarageConcept.Data.Context;
using GarageConcept.Helpers;
using GarageConcept.Models;
using GarageConcept.ViewModels.Orders;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Data.Entity;
using GarageConcept.Services;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;

namespace GarageConcept.Controllers
{
    [Authorize]
    public class OrdersController : BaseController
    {

        private const int ThumbSize = 160;
        
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "ADMIN")]
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CreateOrderViewModel model)
        {
            if (ModelState.IsValid)
            {
                using (var db = new GarageConceptContext())
                {
                    var vehicle = await db.Vehicles.FirstOrDefaultAsync(v => v.PlateNumber == model.Plate);
                    if (vehicle == null)
                        vehicle = db.Vehicles.Add(new Vehicle(model.Plate));
                    vehicle.ChassisNumber = model.Chassis;
                    vehicle.Model = model.VehicleModel;
                    var owner = await db.Users.FirstOrDefaultAsync(u => u.Cpf == model.OwnerCpf) ?? new User(model.OwnerCpf);
                    owner.Name = model.OwnerName;
                    vehicle.Owner = owner;

                    var os = new Order(vehicle);
                    os.Description = model.Description;
                    os.AddToHistory("OS criada");
                    db.Orders.Add(os);

                    await db.SaveChangesAsync();
                }
                return RedirectToAction("Index");
            }
            return View();
        }

        [Authorize(Roles = "ADMIN")]
        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {
            using (var db = new GarageConceptContext())
            {
                var model = await db.Orders.Where(o => o.Id == id)
                    .Select(o => new EditOrderViewModel
                    {
                        Id = o.Id,
                        Chassis = o.Vehicle.ChassisNumber,
                        OwnerCpf = o.OwnerAtOrderDate.Cpf,
                        OwnerName = o.OwnerAtOrderDate.Name,
                        VehicleModel = o.Vehicle.Model,
                        Description = o.Description
                    })
                    .FirstOrDefaultAsync();
                return View(model);
            }
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(EditOrderViewModel model)
        {
            if (ModelState.IsValid)
            {
                using (var db = new GarageConceptContext())
                {
                    var order = await db.Orders.Include(o => o.Vehicle.Owner).Include(o => o.OwnerAtOrderDate).FirstOrDefaultAsync(o => o.Id == model.Id);
                    order.Vehicle.ChassisNumber = model.Chassis;
                    order.Vehicle.Model = model.VehicleModel;
                    order.Description = model.Description;
                    if (order.OwnerAtOrderDate.Cpf != model.OwnerCpf)
                    {
                        var owner = await db.Users.FirstOrDefaultAsync(u => u.Cpf == model.OwnerCpf) ?? new User(model.OwnerCpf);
                        owner.Name = model.OwnerName;
                        order.OwnerAtOrderDate = owner;
                        order.AddToHistory($"OS transferida para {owner.Name}.");
                        if (model.UpdateVehicleOwner)
                        {
                            order.Vehicle.Owner = owner;
                            order.AddToHistory($"{owner.Name} é o novo proprietário do veículo.");
                        }
                    }
                    await db.SaveChangesAsync();
                }
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public async Task<ActionResult> Details(int id)
        {
            using (var db = new GarageConceptContext())
            {
                var q = db.Orders.Where(o => o.Id == id)
                    .Select(o => new OrderDetailsViewModel
                    {
                        Id = o.Id,
                        PlateNumber = o.Vehicle.PlateNumber,
                        Chassis = o.Vehicle.ChassisNumber,
                        VehicleModel = o.Vehicle.Model,
                        OwnerCpf = o.OwnerAtOrderDate.Cpf,
                        OwnerId = o.OwnerAtOrderDate.Id,
                        OwnerName = o.OwnerAtOrderDate.Name,
                        OwnerPhone = o.OwnerAtOrderDate.PhoneNumber,
                        OwnerEmail = o.OwnerAtOrderDate.Email,
                        Description = o.Description,
                        Images = o.Images.Select(i => i.Id)
                    }).AsQueryable();

                bool isAdmin = AuthHelper.IsAdmin();
                if (!isAdmin)
                {
                    var userId = AuthHelper.GetLoggedUserId();
                    q = q.Where(o => o.OwnerId == userId);
                }

                var model = await q.FirstOrDefaultAsync();

                if (model == null)
                    return RedirectToAction("Index");

                model.IsAdmin = isAdmin;

                return View(model);
            }
        }
        
        public ActionResult InternalServerError()
        {
            AddToastrMessage(ToastrStatus.Info, "Ocorreu um erro, mas já estamos trabalhando em uma solução para o mesmo.");
            return View();
        }

        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult> GeneratePassword(int id)
        {
            using (var db = new GarageConceptContext())
            {
                var model = await db.Orders.Where(o => o.Id == id)
                .Select(o => new GeneratePasswordViewModel
                {
                    OrderId = o.Id,
                    PlateNumber = o.Vehicle.PlateNumber
                }).FirstOrDefaultAsync();

                model.Password = PasswordGeneratorService.CreatePassword(6);

                return View(model);
            }
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> GeneratePassword(GeneratePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                using (var db = new GarageConceptContext())
                {
                    var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

                    var owner = await db.Orders.Where(o => o.Id == model.OrderId).Select(o => o.OwnerAtOrderDate).FirstAsync();
                    IdentityResult passwordChangeResult;
                    if (string.IsNullOrWhiteSpace(owner.PasswordHash))
                    {
                        passwordChangeResult = await userManager.AddPasswordAsync(owner.Id, model.Password);
                    }
                    else
                    {
                        string resetToken = await userManager.GeneratePasswordResetTokenAsync(owner.Id);
                        passwordChangeResult = await userManager.ResetPasswordAsync(owner.Id, resetToken, model.Password);
                    }
                    if (passwordChangeResult.Succeeded)
                    {
                        AddToastrMessage(ToastrStatus.Success, "Senha atualizada com sucesso!");
                        return RedirectToAction("Index");
                    }
                    AddErrors(passwordChangeResult);
                }
            }
            return View(model);
        }

    }
}