﻿$(function () {

    var getUrl = $("#grid-orders").data("get-url");
    var deleteUrl = $("#grid-orders").data("delete-url");
    var editUrl = $("#grid-orders").data("edit-url");
    var detailsUrl = $("#grid-orders").data("details-url");

    var grid = $("#grid-orders").bootgrid({
        ajax: true,
        rowCount: [10, 25, 50, 100],
        labels: {
            all: "Todos",
            infos: "Mostrando de {{ctx.start}} a {{ctx.end}} para um total de {{ctx.total}} OS",
            loading: "Carregando...",
            refresh: "Atualizar",
            search: "Pesquisar",
            noResults: "Nenhuma OS encontrada..."
        },
        requestHandler: function (request) {
            request.sortItems = [];
            if (request.sort === null)
                return request;
            for (var property in request.sort) {
                if (request.sort.hasOwnProperty(property)) {
                    request.sortItems.push({ Field: property, Type: request.sort[property] });
                }
            }
            return request;
        },
        ajaxSettings: {
            method: "POST",
            cache: false
        },
        searchSettings: {
            delay: 250,
            characters: 3
        },
        url: getUrl,
        formatters: {
            "vehicle": function (column, row) {
                return `
                    <div class="row">
                        <div class="col-xs-12">
                            <span data-masked="" data-inputmask="'mask': 'aaa-9999'">${row.PlateNumber}</span> | ${row.Model}
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="row">
                            Chassi: ${row.ChassisNumber}
                        </div>
                    </div>
                `;
            },
            "status": function (column, row) {
                return `
                    <div class="row">
                        <div class="col-xs-12">
                            ${row.StatusDisplay}
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="row">
                            Data: ${row.RegisterDisplay}
                        </div>
                    </div>
                `;
            },
            "owner": function (column, row) {
                console.log(row);
                return `
                    <div class="row">
                        <div class="col-xs-12">
                            ${row.OwnerName} - CPF: <span data-masked="" data-inputmask="'mask': '999.999.999-99'"> ${row.OwnerCpf}</span>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="row">
                            ${row.OwnerEmail}
                        </div>
                    </div>
                `;
            },
            "commands": function (column, row) {
                var admin = $('#flags').data('admin');
                if (admin == "True") {
                    var str = `
                <button type="button" data-toggle="tooltip" data-placement="top" title="Ver detalhes" class="btn btn-xs btn-primary command-details" data-row-id="${row.Id}"><span class="fa fa-search"></span></button>
                <button type="button" data-toggle="tooltip" data-placement="top" title="Editar" class="btn btn-xs btn-info command-edit" data-row-id="${row.Id}"><span class="fa fa-edit"></span></button>
                <button type="button" data-toggle="tooltip" data-placement="top" title="Excluir" class="btn btn-xs btn-danger command-delete" data-row-id="${row.Id}"><span class="fa fa-trash"></span></button>
                `;
                    return str;
                }
                else {
                    return `<button type="button" data-toggle="tooltip" data-placement="top" title="Ver detalhes" class="btn btn-xs btn-primary command-details" data-row-id="${row.Id}"><span class="fa fa-search"></span></button>`;
                }
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {

        $('[data-toggle="tooltip"]').tooltip();

        if ($.fn.inputmask)
            $('[data-masked]').inputmask();

        grid.find(".command-edit").mouseup(function (e) {
            var url = editUrl;
            result = url.substring(0, url.lastIndexOf("/"));
            url = result + '/' + $(this).data("row-id");
            if (e.which === 1)
                document.location = url;
            else if (e.which === 2)
                window.open(url, "_blank");
        });

        grid.find(".command-details").mouseup(function (e) {
            var url = detailsUrl;
            result = url.substring(0, url.lastIndexOf("/"));
            url = result + '/' + $(this).data("row-id");
            if (e.which === 1)
                document.location = url;
            else if (e.which === 2)
                window.open(url, "_blank");
        });

        grid.find(".command-delete").mouseup(function (e) {
            var url = deleteUrl;
            url = url.substring(0, url.lastIndexOf("/"));
            url = url + '/' + $(this).closest('tr').data("row-id");
            swal({
                title: "Deseja excluir essa OS?",
                text: "",
                type: "warning",
                showCancelButton: true,
                //confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, desejo excluir!",
                cancelButtonText: "Não!",
                closeOnConfirm: true,
                closeOnCancel: true
            },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: url,
                            method: "DELETE"
                        })
                            .done(function () {
                                grid.bootgrid('reload');
                                toastr.success("OS excluída com sucesso!");
                            })
                            .fail(function () {
                                toastr.error("Não foi possível excluir essa OS!");
                            });
                    }
                });
        });

    });


});