﻿$(function () {

    var getPlateDetailsUrl = $("#urls").data("plate-details-url");
    var getUserDetailsUrl = $("#urls").data("user-details-url");

    $(".plate").keyup(function () {
        var plateNumber = $(this).inputmask('unmaskedvalue');
        if (plateNumber.length === 7) {
            $.ajax({
                url: getPlateDetailsUrl.replace("ZZZ9999", plateNumber),
                method: "GET"
            })
                .done(function (data) {
                    $("#Chassis").val(data.ChassisNumber);
                    $("#VehicleModel").val(data.Model);
                    $("#OwnerCpf").val(data.Cpf);
                    $("#OwnerName").val(data.Name);
                });
        }
    });

    $(".cpf").keyup(function () {
        var cpf = $(this).inputmask('unmaskedvalue');
        if (cpf.length === 11) {
            $.ajax({
                url: getUserDetailsUrl.replace("99999999999", cpf),
                method: "GET"
            })
                .done(function (data) {
                    $("#OwnerName").val(data.Name);
                });
        }
    });

});